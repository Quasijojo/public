# On basic income and asset taxation
## by Caleb James DeLisle, @cjd@mastodon.social

The following is from an email sent to a mailing list some time ago, preserved in original form.

    Funny you should mention that because I've been searching for a
    stable political/economic system and I feel like I've found one
    which is not only stable but highly infectous once implemented.

    I don't have optimal numbers in mind but here is the basic idea:

    1. Basic Income - Not just to help the poor or because it's "good",
    it plays a critical role the functioning of the economy because it
    makes people spend.

    2. Hard Asset Tax - This is designed to prevent the establishment
    of rentir dynasties which sap the wealth from the economy. It has
    a side-effect that asset values go down because people need to
    unload their property onto someone who is making good use of it.
    Intellectual Property, radio spectrum, mining rights, right-of-way,
    money/bitcoin and money loans are all included, stocks are not.

    3. Import Terrif - applied against states which do not offer
    Basic Income OR who do not use an equivilant terrif. This is just
    designed to prevent Basic Income leaking away to other states which
    implement policies to impoverish their citizens thus allowing them
    to produce exports at a cheaper price. Note that it is like the GPL
    of terrifs, anyone get in but they need to impose the same rule.
    Revenues of the terrif are divided evenly between all countried who
    implement it.

    4. Raw materials and technology (computers) for businesses are
    subsidized to maintain price stability. Stability of currency
    trading price is NOT enforced.

    5. No revenue or VAT taxes.



    This is really rough at this point but my gut feeling is this will
    trigger the following:

    A. People have pocket money so they spend it, small/local
    businesses fluorish.

    B. Rental Property becomes a worse investment than stake in main
    street businesses, housing bubble bursts and a place to sleep
    ceases to be a significant part of peoples' monthly bills.

    C. Import terrif immediately has the effect of improving the
    market for local businesses but other nations pass retaliatory
    terrifs, multinationals seriously harmed by terrifs pull out
    immediately causing collapse in certain market sectors.

    D. Enticed by captive market (by terrif), cheap land and assets,
    and no revenue, VAT or stockholder tax, nervous foreigners begin
    to invest in local companies. As the economy grows and becomes
    better known, this flow of capital becomes a torrent.

    E. Spook event causes mass exodus of panicey foreign capital
    (South-East Asia 1998) however currency value is NOT explicitly
    protected, currency trade value plummits and foreign investors
    seeking exit lose most of their investment. Foreign goods spike
    in price but protected goods (raw materials etc. for business)
    remain stable. Wiser investors stay put and without any serious
    cause, currency trading rate springs back.

    F. As the economy grows, other markets will consider adopting
    similar policies in order to bypass the terrif and sell into the
    market. Technically they can do this by implementing only the
    "viral" terrif but if they collect state finance by VAT then
    their businesses will be at a significant competive disadvantage
    to ones which pay no tax except on hard assets so the natural
    tenancy will be to implement the whole regime of rules. Upon
    doing that, the other countries immediately become an effective
    trade union without the need to sign any treaty which makes the
    system more powerful.

    G. Different nations jumping on the band-wagon will prioritize
    different sets of goods to subsidize, this is kept fair because
    the terrif revenues are divided evenly so it is not economically
    interesting to subsidize goods (effectively ducking the terrif)
    and then export them to another nation in the union.

    H. After many nations are using these rules, deviations put the
    devient at an economic disadvantage (TODO: verify) so most
    political attempts to subvert this system will fail.

    Ok that wasn't meant to be so long but I suppose you get the
    general idea. I'm still playing with it...

