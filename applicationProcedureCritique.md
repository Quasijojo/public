# Joachim's critique of §7. Application Procedure (Constitution)
## paraphrased according to Doug
### Summary
The current procedure allows a minority (>25%) to reject any applicant regardless of all other opinion. This favors irrational, emotionally impulsive and/or intolerant characteristics will lead to the group becoming progressively dysfunctional and eventually collapsing: project failure.

### Reasoning
Fact 1: All decisions in the procedure are binary choices between progressing one Position up or being reevaluated at the current Position. For example, a Visitor applying to become a Volunteer would become a Volunteer if successful or be reevaluated as a Visitor if unsuccussful: a Visitor being reevluated as a Visitor would remain as a Visitor if successful or be recommended to leave if unsuccessful.

Fact 2: One person voting 'oppose' counteracts three people voting 'support' (and an infinite number of people voting 'accept',) therefore a minority (>25%) is able to reject any applicant.

Fact 3: All individuals are eventually reevaluated according to the same procedure.

Assumption 1: Applicants with dysfunctional characteristics are often more charismatic—and therefore less likely to be opposed—than applicants with desirable characteristics.

Assumption 2: Voters with desirable characteristics are less likely to oppose applicants than voters with dysfunctional characteristics.

Thus a problem arises for a group of individuals with desirable characteristics **if**—
  - they accept individuals with dys. char.s to >25% of the group membership, and
  - voters with dys. char.s oppose applicants with des. char.s with greater than 1/3 the frequency that voters with des. char.s oppose applicants with dys. char.s.

**Then** a runaway scenario occurs in which the group becomes progressively populated by individuals with dysfunctional characteristics.

This problem is accelerated since existing Volunteers and Members are reevaluated.

---

_The following is copy pasted from https://kanthaus.online/en/governance/constitution_

## §7. Application Procedure
1. The applicant must not be present during the procedure.
1. Firstly participants should take a preliminary vote in which they anonymously express whether they ‘support’ (+), ‘accept’ (0) or ‘oppose' (-) the application succeeding.
1. Secondly participants should discuss the application and, in particular, raise and address any concerns.
1. Finally participants should take a binding vote in the same fashion as the preliminary vote. This time—
	1. if there are 3 or more times as many ‘support’ votes to ‘oppose’ votes, the application succeeds, or
	1. else the application fails at that time.
1. The applicant should promptly be informed whether their application has succeeded or failed, but not the breakdown of which votes were cast.
