<!-- PRE -->

**Introducing Kanthaus**

Doug Webb, Dec 2017

_Talk to me: @douginamug on [Mastodon.xyz](https://mastodon.xyz/@douginamug) and [Twitter](https://twitter.com/DougInAMug)_

---

<!-- 0.00 -->
<!-- yunity → foodsharing → needed to get a place → WE DECIDED TO BUY A PLACE -->

**Kanthaus**

Self-owned, cooperative common-house in former East Germany, promoting sharing and preventing waste.

---?image=http://www.powerpointhintergrund.com/uploads/history-ppt-background-0.jpg 

<img alt="Group picture from first Wuppdays in Malo" src="http://www.raphaelfellmer.de/wp-content/uploads/2016/02/raphael_fellmer08klein.jpg" style="height:75vh;border:0;box-shadow:none;">

---?image=http://www.powerpointhintergrund.com/uploads/history-ppt-background-0.jpg 

<img alt="Baddu, group around bin fire." src="https://yunity.org/en/heartbeat/2016-12-11/outdoorbreakfast.jpg" style="height:75vh;border:0;box-shadow:none;">

---

<!-- 0.59 -->
<!-- Kantstraße 20 & 22, Wurzen (30 km East of Leipzig) - English inside, German outside. Built ~1890. ~400m^2 room space (~ 30 rooms,) ~200m^2 basement/attic space. €41k: popularity & repairwork (bitumen, rot, electrics, pipework, utilities. Donations/loans from 7 people, owned as coop → HOW WE WORK INTERNALLY -->

<img alt="Street view of Kanthaus buildings." src="https://kanthaus.online/pics/wurzenfront.jpg" style="height:75vh;border:0;box-shadow:none;">

---

<img alt="Ground floor diagram." src="https://assets.gitlab-static.net/kanthaus/kanthaus-public/raw/master/larasRoomSuggestions/0_Erdgeschoss.jpg" style="height:75vh;border:0;box-shadow:none;">

---

<img alt="Initial finance pie chart" src="https://assets.gitlab-static.net/DougInAMug/public/raw/master/kanthaus/financePie.png" style="height:75vh;border:0;box-shadow:none;">

--- 

<img alt="Kanthaus Entities and Relations" src="https://kanthaus.online/pics/basicKanthausER.svg" style="height:75vh;border:0;box-shadow:none;">

---

<!-- 1.59 -->
<!--
- The project
  - inspiration from coops, commons, permaculture, agile and the scientific method
  - governance in 3 tiers: community constitution (purposes, positions and procedures core decision-making + conflict res,) secondary agreements (smoking, drinking, cleaning hour, etc,) autonomous action/doocracy
  - 3 positions: visitor: anyone can come as a visitor, minimal rights/responsbilities, evaluate after 2 weeks, volunteers: can apply after 2 weeks as vis, medium rights/responsibilities, evaluate after 3 months, member: can apply after one month as vol, full rights/reponsibilities, evaluation tbc
  - important: no veto.
  - WHERE WE GOING NEXT
-->
  
<img alt="Waterfall vs Agile" src="https://static1.squarespace.com/static/511269bbe4b0c73df72dc118/t/59a714d6e3df28f2da211186/1504122071983/waterfall-and-agile-methods.jpg" style="height:75vh;border:0;box-shadow:none;">

---

**Organization**

- Documents: community constitution & secondary agreements
- 3 positions: visitor, volunteer & member
- Decisions: autonomous → 'consensus' → score voting
- Conflict: direct → assisted → intervention
- Meetings: weekly coordination, fortnightly sharing, quarterly roadmap

---

<!-- 2.59 -->
**Future**

- 'Permanently' demarketize the house
- Start free-shop & kitchen-for-all
- Gardening at allotments
- Pull in > €10k p.a.
- Host first hackathon
- Replace roof
- ...

---

**Let's cooperate**

- [kanthaus.online](https://kanthaus.online)
- Come visit 
  - n.b. not super supportive atmosphere atm
- Spread the word
- Enquire about software
- Keep us accountable

<!-- 3.59 -->
<!-- Questions and answers -->
  
























 
