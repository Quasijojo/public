<<<<<<< HEAD
https://gitpitch.com/DougInAMug/public/master?p=cryptoUD
=======
# In progress

First draft [here](https://docs.google.com/presentation/d/1wMDBaOnaIOojTXTe1Zs-Zc8R4Vr_sPrxbhRHY5V6sVs/edit?usp=sharing)
>>>>>>> 05ba6dd4a1a9b5ccdf0eac464b88dc1bdfcf305b

---

# Crypto- Universal Dividend

Doug Webb, 2017

Talk to me on [Mastodon](https://mastodon.xyz/@douginamug) or [Twitter](https://twitter.com/DougInAMug)

---

### What do we all want?

![Video](https://www.youtube.com/embed/WHBQC0XYvYg?start=95)

---

### What do we all want?

- No one knows!
  - You can’t talk to everyone
  - You’re not telepathic
- Two methods are used to estimate
  - **Voting**
  - **Currency** 
 
---

### Voting
 
- Specific people
- Specific situation
- Synchronous
- Minor role in economy
- Facilitates planned action

---

### Voting: uniform distribution

<image of a labelled uniform distribution>

---

### Currency

- Any person(s)
- Any situation(s)
- Asynchronous
- Major role in economy
- Facilitates spontaneous action
- Most currency is fiat money (£, $, €, etc)
  - Centralized creation and initial distribution by banks (non-democratic)
  - Ultimately enforced by state (through taxes, etc)

---

### Money (£$€): exponential distribution

<image of a labelled uniform distribution>

---



<div style="text-align: center"><b>
Not having any currency  
===  ↕
Not being able to vote  
===  ↕
You don’t count
</b></div>

---

### What about Bitcoin!

![Image](https://bitcoin.org/img/icons/opengraph.png)

---

### Bitcoin

- Blockchain and other technological wizardry allows for the creation of digital currencies—
  - Without banks (generated algorithmically)
  - Without state enforcement (voluntary)
  - Without centralization (blockchain)
- Bitcoin, the poster cryptocurrency, now has a net market value at over $200 Bn
- However... |

---

### Bitcoin consumes loads of power

- As much as Croatia or Slovakia or Nigeria.
- 27 times as much as the global Visa Network

---

### 
